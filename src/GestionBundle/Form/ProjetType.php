<?php

namespace GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProjetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('intituleProjet', TextType::class, array('label' => 'Intitulé projet'));
        $builder->add('description', TextType::class, array('label' => 'Description'));
        $builder->add('type', ChoiceType::class, array('choices' => array(
                                                          'Capex' => 'CAPEX',
                                                          'Opex' => 'OPEX', )));
        $builder->add('dateCreationProjet', DateType::class, array('label' => 'DateCreationProjet'));
        $builder->add('livre', ChoiceType::class, array('choices' => array(
                                                           'Oui' => 1,
                                                           'Non' => 0, )));
        $builder->add('archiveProjet', ChoiceType::class, array('choices' => array(
                                                          'Oui' => 1,
                                                          'Non' => 0, )));
    }
}
