<?php

namespace GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EmployeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', TextType::class, array('label' =>'Nom'));
        $builder->add('prenom', TextType::class, array('label' =>'Prénom'));
        $builder->add('email', EmailType::class, array('label' =>'Email'));
        $builder->add('coutJournalier', NumberType::class, array('label' =>'Cout journalier (en €)'));
        $builder->add('archive', ChoiceType::class, array('choices' => array(
                                                           'Oui' => 1,
                                                           'Non' => 0, )));
        $builder->add('dateEmbauche', DateType::class, array('label' =>'Date d\'embauche'));
        $builder->add('metier', EntityType::class, array('label'        => 'Metier',
                                                         'class'        => 'GestionBundle\Entity\Metier',
                                                         'choice_label' => 'intituleMetier',
                                                         'multiple'     => false));
    }


}
