<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use GestionBundle\Entity\Employe;
use GestionBundle\Entity\Projet;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\CoupProductionRepository")
 * @ORM\Table(name="coup_production")
 */

class CoupProduction
{

  /**
   * @ORM\Column(name="id_coup", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $idCoup;

  /**
   * @ORM\ManyToOne(targetEntity="Employe")
   * @ORM\JoinColumn(nullable=false)
   */
  private $employe;

  /**
   * @ORM\ManyToOne(targetEntity="Projet")
   * @ORM\JoinColumn(nullable=false)
   */
  private $project;

  /**
   * @ORM\Column(name="jour_production", type="integer", nullable=false)
   */
  private $jourProduction;

  /**
   * @ORM\Column(name="date_production", type="datetime", nullable=false)
   */
  private $dateProduction;

  // constructeur

  function __construct(){
      $this->dateProduction = new \DateTime();
    }

  // Getters and Setters

  /**
   * Get idCoup
   */
  public function getIdCoup()
  {
    return $this->idCoup;
  }

  /**
   * Set idCoup
   *
   * @return self
   */
  public function setIdCoup($idCoup)
  {
    $this->idCoup = $idCoup;
    return $this;
  }

  /**
   * Get employe
   */
  public function getEmploye()
  {
    return $this->employe;
  }

  /**
   * Set employe
   *
   * @return self
   */
  public function setEmploye($employe)
  {
    $this->employe = $employe;
    return $this;
  }

  /**
   * Get project
   */
  public function getProjet()
  {
    return $this->project;
  }

  /**
   * Set project
   *
   * @return self
   */
  public function setProjet($project)
  {
    $this->project = $project;
    return $this;
  }

  /**
   * Get jourProduction
   */
  public function getJourProduction()
  {
    return $this->jourProduction;
  }

  /**
   * Set jourProduction
   *
   * @return self
   */
  public function setJourProduction($jourProduction)
  {
    $this->jourProduction = $jourProduction;
    return $this;
  }
}

 ?>
