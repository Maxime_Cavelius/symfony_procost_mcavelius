<?php

namespace GestionBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\MetierRepository")
 * @ORM\Table(name="metier")
 */
class Metier
{

  /**
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $idMetier;

  /**
   * @ORM\Column(name="intitule_metier", type="string", length=100, nullable=false)
   * @Assert\NotBlank(message="le nom est obligatoire")
   */
  private $intituleMetier;

  // Getters and Setters

  /**
   * Get Id
   */
   public function getIdMetier()
   {
     return $this->idMetier;
   }

   /**
    * Set Id
    *
    * @return self
    */
    public function setIdMetier($idMetier)
    {
      $this->idMetier = $idMetier;

      return $this;
    }

   /**
    * Get IntituleMetier
    */
    public function getIntituleMetier()
    {
      return $this->intituleMetier;
    }

    /**
     * Set IntituleMetier
     *
     * @return self
     */
     public function setIntituleMetier($intituleMetier)
     {
       $this->intituleMetier = $intituleMetier;

       return $this;
     }

}
 ?>
