<?php

namespace GestionBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


  /**
   * @ORM\Entity(repositoryClass="GestionBundle\Repository\ProjetRepository")
   * @ORM\Table(name="projet")
   */

class Projet
{

  //Déclaration des variables

  /**
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $idProjet;

  /**
   * @ORM\Column(name="intitule_projet", type="string", length=50, nullable=false)
   * @Assert\NotBlank(message="Le nom est obligatoire")
   */
  private $intituleProjet;

  /**
   * @ORM\Column(name="description", type="text", nullable=false)
   * @Assert\NotBlank()
   */
  private $description;

  /**
   * @ORM\Column(name="type", type="string", length=5, nullable=false)
   * @Assert\NotBlank()
   */
  private $type;

  /**
   * @ORM\Column(name="date_creation_projet", type="datetime", nullable=false)
   * @Assert\NotBlank()
   */
  private $dateCreationProjet;

  /**
   * @ORM\Column(name="livre", type="boolean", options={"default" = 0}, nullable=false)
   * @Assert\NotBlank()
   */
  private $livre;

  /**
   * @ORM\Column(name="archive_projet", type="boolean", options={"default" = 0}, nullable=false)
   * @Assert\NotBlank()
   */
  private $archiveProjet;

  // constructeur

  function __construct(){
      $this->dateCreationProjet = new \DateTime();
    }

  // Getters and Setters

  /**
   * Get idProjet
   */
   public function getIdProjet()
   {
     return $this->idProjet;
   }


  /**
   *
   * Set idProjet
   * @return self
   */
  public function setIdProjet($idProjet)
  {
    $this->idProjet = $idProjet;
    return $this;
  }

  /**
   * Get intituleProjet
   */
  public function getIntituleProjet()
  {
    return $this->intituleProjet;
  }

  /**
   * Set intituleProjet
   *
   * @return self
   */
  public function setIntituleProjet($intituleProjet)
  {
    $this->intituleProjet = $intituleProjet;
    return $this;
  }

  /**
   * Get description
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   *Set decription
   *
   * @return self
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Get type
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set type
   *
   * @return self
   */
  public function setType($type)
  {
    $this->type = $type;
    return $this;
  }

  /**
   * Get livre
   */
  public function getLivre()
  {
    return $this->livre;
  }

  /**
   * Set livre
   *
   * @return self
   */
  public function setLivre($livre)
  {
    $this->livre = $livre;
    return $this;
  }

  /**
   * Get archiveProjet
   */
  public function getArchiveProjet()
  {
    return $this->archiveProjet;
  }

  /**
   * Set archiveProjet
   *
   * @return self
   */
  public function setArchiveProjet($archiveProjet)
  {
    $this->archiveProjet = $archiveProjet;
    return $this;
  }

  /**
   * Get dateCreationProjet
   */
  public function getDateCreationProjet()
  {
    return $this->dateCreationProjet;
  }

  /**
   * Set dateCreationProjet
   *
   * @return self
   */
  public function setDateCreationProjet($dateCreationProjet)
  {
    $this->dateCreationProjet = $dateCreationProjet;
    return $this;
  }

}
?>
