<?php

namespace GestionBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use GestionBundle\Entity\Metier;

  /**
   * @ORM\Entity(repositoryClass="GestionBundle\Repository\EmployeRepository")
   * @ORM\Table(name="employe")
   */

class Employe
{

  //Déclaration des variables

  /**
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $idEmploye;

  /**
   * @ORM\Column(name="nom", type="string", length=50, nullable=false)
   */
  private $nom;

  /**
   * @ORM\Column(name="prenom", type="string", length=50, nullable=false)
   */
  private $prenom;

  /**
   * @ORM\Column(name="email", type="string", length=255, nullable=false)
   */
  private $email;

  /**
   * @ORM\ManyToOne(targetEntity="Metier")
   * @ORM\JoinColumn(nullable=false)
   * @Assert\Valid()
   * @Assert\Type(type="GestionBundle\Entity\Metier")
   */
  private $metier;

  /**
   * @ORM\Column(name="cout_journalier", type="float", nullable=false)
   */
  private $coutJournalier;

  /**
   * @ORM\Column(name="date_embauche", type="datetime", nullable=false)
   */
  private $dateEmbauche;

  /**
   * @ORM\Column(name="archive", type="boolean", options={"default" = 0}, nullable=false)
   */
  private $archive;

  // constructeur

  function __construct(){
      $this->dateEmbauche = new \DateTime();
    }

  // Getters and Setters

  /**
   * Get IdEmploye
   */
  public function getIdEmploye()
  {
    return $this->idEmploye;
  }

  /**
   * Set IdEmploye
   *
   * @return self
   */
  public function setIdEmploye($idEmploye)
  {
    $this->idEmploye = $idEmploye;
    return $this;
  }

  /**
   * Get nom
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Set nom
   *
   * @return self
   */
  public function setNom($nom)
  {
    $this->nom = $nom;
    return $this;
  }

  /**
   * Get prenom
   */
  public function getPrenom()
  {
    return $this->prenom;
  }

  /**
   * Set Prenom
   *
   * @return self
   */
  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;
    return $this;
  }

  /**
   * Get email
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set email
   *
   * @return self
   */
  public function setEmail($email)
  {
    $this->email = $email;
    return $this;
  }

  /**
   * Get coutJournalier
   */
  public function getCoutJournalier()
  {
    return $this->coutJournalier;
  }

  /**
   * Set coutJournalier
   *
   * @return self
   */
  public function setCoutJournalier($coutJournalier)
  {
    $this->coutJournalier = $coutJournalier;
    return $this;
  }

  /**
   * Get metier
   */
  public function getMetier()
  {
    return $this->metier;
  }

  /**
   * Set Metier
   *
   * @return self
   */
  public function setMetier($metier)
  {
    $this->metier = $metier;
  }

  /**
   * Get archive
   */
  public function getArchive()
  {
    return $this->archive;
  }

  /**
   * Set archive
   *
   * @return self
   */
  public function setArchive($archive)
  {
    $this->archive = $archive;
  }

  /**
   * Get dateEmbauche
   */
  public function getDateEmbauche()
  {
    return $this->dateEmbauche;
  }

  /**
   * Set dateEmbauche
   *
   * @return self
   */
  public function setDateEmbauche($dateEmbauche)
  {
    $this->dateEmbauche = $dateEmbauche;
  }

}
?>
