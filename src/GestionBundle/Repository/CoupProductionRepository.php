<?php

namespace GestionBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CoupProductionRepository extends  EntityRepository {

    public function allJourProduction(){
      $qb = $this->createQueryBuilder('cp');

      $qb ->select('count(cp)');

      $res = $qb  ->getQuery()
                  ->getResult();

      return $res[0][1];
    }

    public function meillerEmploye(){

      $query = $this->_em->createQuery('SELECT e.idEmploye, e.nom, e.prenom, e.dateEmbauche, SUM(e.coutJournalier * cp.jourProduction) as total
                                        FROM GestionBundle:Employe e, GestionBundle:CoupProduction cp
                                        WHERE cp.employe = e.idEmploye
                                        AND e.archive = 0
                                        GROUP BY e.idEmploye
                                        ORDER BY total DESC')->setMaxResults(1);

      $results = $query->getResult();
      return $results[0];
    }

    public function lastProjet(){
      $query =$this->_em->createQuery('SELECT p.idProjet, p.intituleProjet, p.type, p.dateCreationProjet, p.livre, SUM(e.coutJournalier*cp.jourProduction) as total
                                       FROM GestionBundle:Projet p, GestionBundle:Employe e, GestionBundle:CoupProduction cp
                                       WHERE p.idProjet = cp.project
                                       AND e.idEmploye = cp.employe
                                       AND p.archiveProjet = 0
                                       GROUP BY p.idProjet
                                       ORDER BY p.dateCreationProjet DESC')->setMaxResults(5);

      $result = $query->getResult();
      return $result;
    }

    public function lastTempSaisie(){
      $query =$this->_em->createQuery('SELECT e.nom, e.prenom, m.intituleMetier, cp.jourProduction, cp.dateProduction
                                       FROM GestionBundle:Projet p, GestionBundle:Employe e, GestionBundle:CoupProduction cp, GestionBundle:Metier m
                                       WHERE e.idEmploye = cp.employe
                                       AND p.idProjet = cp.project
                                       AND m.idMetier = e.metier
                                       ORDER BY cp.dateProduction DESC')->setMaxResults(10);

      $result = $query->getResult();
      return $result;
    }

}
