<?php

namespace GestionBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EmployeRepository extends  EntityRepository {

    public function allEmploye(){
      $query = $this->_em->createQuery('SELECT e.idEmploye, e.nom, e.prenom, e.email, e.dateEmbauche, e.coutJournalier, m.intituleMetier
                                        FROM GestionBundle:Employe e, GestionBundle:Metier m
                                        WHERE e.metier = m.idMetier
                                        ORDER BY e.nom');

      $result = $query->getResult();
      return $result;
    }

    public function employeEnregistre(){
      $qb = $this->createQueryBuilder('e');

      $qb ->select('count(e)')
          ->where('e.archive = 0');

      $res = $qb  ->getQuery()
                  ->getResult();

      return $res[0][1];
    }

    public function suiviEmploye($id){
      $query = $this->_em->createQuery('SELECT e.nom, e.prenom, e.email, e.coutJournalier, e.dateEmbauche, m.intituleMetier, m.idMetier
                                        FROM GestionBundle:Employe e, GestionBundle:Metier m
                                        WHERE e.idEmploye = '.$id.'
                                        AND m.idMetier = e.metier');

      $result = $query->getResult();
      return $result[0];
    }

    public function suiviEmployeProjet($id){
      $query = $this->_em->createQuery('SELECT p.idProjet, p.intituleProjet, cp.jourProduction, cp.dateProduction, SUM(cp.jourProduction * e.coutJournalier) as total
                                        FROM GestionBundle:Projet p, GestionBundle:Employe e, GestionBundle:CoupProduction cp
                                        WHERE e.idEmploye = '.$id.'
                                        AND p.idProjet = cp.project
                                        AND e.idEmploye = cp.employe
                                        GROUP BY p.idProjet');

      $result = $query->getResult();
      return $result;
    }

    public function getMetierby($id){
      $query = $this->_em->createQuery('SELECT m.intituleMetier, m.idMetier
                                        FROM GestionBundle:Metier m
                                        WHERE m.idMetier = '.$id.'');

      $result = $query->getResult();
      return $result;
    }

}
