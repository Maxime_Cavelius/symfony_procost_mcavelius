<?php

namespace GestionBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProjetRepository extends  EntityRepository {

    public function allProjet(){
      $query = $this->_em->createQuery('SELECT p.intituleProjet, p.description, p.type, p.dateCreationProjet, p.livre, p.idProjet
                                        FROM GestionBundle:Projet p
                                        WHERE p.archiveProjet = 0
                                        ORDER BY p.intituleProjet');

      $result = $query->getResult();
      return $result;
    }

    public function projetEnRealisation(){
      $qb = $this->createQueryBuilder('p');

      $qb ->select('count(p)')
          ->where('p.livre = 0 AND p.archiveProjet = 0');

      $res = $qb  ->getQuery()
                  ->getResult();

      return $res[0][1];
    }

    public function projetLivre(){
      $qb = $this->createQueryBuilder('p');

      $qb ->select('count(p)')
          ->where('p.livre = 1 AND p.archiveProjet = 0');

      $res = $qb  ->getQuery()
                  ->getResult();

      return $res[0][1];
    }

    public function projetNonLivre(){
      $qb = $this->createQueryBuilder('p');

      $qb ->select('count(p)')
          ->where('p.livre = 0 AND p.archiveProjet = 0');

      $res = $qb  ->getQuery()
                  ->getResult();

      return $res[0][1];
    }

    public function projetCapex(){
      $qb = $this->createQueryBuilder('p');

      $qb ->select('count(p)')
          ->where('p.type = :type AND p.archiveProjet = 0')
          ->setParameter('type','CAPEX');

      $res = $qb  ->getQuery()
                  ->getResult();

      return $res[0][1];
    }

    public function projetOpex(){
      $qb = $this->createQueryBuilder('p');

      $qb ->select('count(p)')
          ->where('p.type = :type AND p.archiveProjet = 0')
          ->setParameter('type','OPEX');

      $res = $qb  ->getQuery()
                  ->getResult();

      return $res[0][1];
    }

    public function projetRentabilite($capex, $opex){
      $totalRentabilite = $capex + $opex;

      $tauxCapex = $capex/$totalRentabilite * 100;
      $tauxOpex = $opex/$totalRentabilite * 100;

      return array('opex' => $tauxOpex, 'capex' => $tauxCapex);
    }

    public function projetLivraison($livre, $nonLivre){
      $totalLivraison = $livre + $nonLivre;

      $tauxLivre = $livre/$totalLivraison * 100;
      $tauxNonLivre = $nonLivre/$totalLivraison * 100;

      return array('livre' => $tauxLivre, 'nonLivre' => $tauxNonLivre);
    }

    public function projetToEmploye($id){
      $query = $this->_em->createQuery('SELECT p.idProjet, p.intituleProjet, p.archiveProjet, p.livre
                                        FROM GestionBundle:Projet p, GestionBundle:Employe e, GestionBundle:CoupProduction cp
                                        WHERE e.idEmploye = cp.employe
                                        AND p.idProjet = cp.project
                                        AND e.idEmploye = '.$id.'');

      $result = $query->getResult();
      return $result;
    }

    public function projetById($id){
      $query = $this->_em->createQuery('SELECT p.intituleProjet, p.description, p.type, p.dateCreationProjet, p.livre
                                        FROM GestionBundle:Projet p
                                        WHERE p.idProjet = '.$id.'');

      $result = $query->getResult();
      return $result[0];
    }

    public function projetEmploye($id){
      $query = $this->_em->createQuery('SELECT e.nom, e.prenom, e.idEmploye
                                        FROM GestionBundle:Projet p, GestionBundle:Employe e, GestionBundle:CoupProduction cp
                                        WHERE e.idEmploye = cp.employe
                                        AND p.idProjet = cp.project
                                        AND p.idProjet = '.$id.'');

      $result = $query->getResult();
      return $result;
    }

    public function projetTempProd($id){
      $query = $this->_em->createQuery('SELECT e.nom, e.prenom, e.idEmploye, cp.jourProduction, SUM(cp.jourProduction*e.coutJournalier) as total
                                        FROM GestionBundle:Projet p, GestionBundle:Employe e, GestionBundle:CoupProduction cp
                                        WHERE e.idEmploye = cp.employe
                                        AND p.idProjet = cp.project
                                        AND p.idProjet = '.$id.'
                                        GROUP BY e.idEmploye');

      $result = $query->getResult();
      return $result;
    }

}
