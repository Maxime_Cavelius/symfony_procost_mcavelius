<?php

namespace GestionBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MetierRepository extends  EntityRepository {

    public function allMetier(){
      $query = $this->_em->createQuery('SELECT m.idMetier, m.intituleMetier
                                        FROM GestionBundle:Metier m
                                        ORDER BY m.idMetier');

      $result = $query->getResult();
      return $result;
    }

    

}
