<?php

namespace GestionBundle\DataFixture;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use GestionBundle\Entity\Employe;
use GestionBundle\Entity\Metier;
use GestionBundle\Entity\Projet;
use GestionBundle\Entity\CoupProduction;

class GestionFixtures extends Fixture
{
  public function load(ObjectManager $manager)
  {
    //table de nom
    $nom = array('Cavelius', 'Vilvots', 'Perrin', 'Richard', 'Petit', 'Michel', 'Robert', 'Simon', 'Blanc', 'Dupont', 'Dupond');

    //table de prenom
    $prenom = array('Louise', 'Gabriel', 'Emma', 'Raphäel', 'Jade', 'Jules', 'Chloé', 'Léo', 'Alice', 'Lucas', 'Léa');

    //table de métier
    $metier = array('SEO Manager', 'Digital Manager', 'WEB Designer', 'WEB Developer');

    //type projets
    $type = array('OPEX','CAPEX');

    //intitule projet
    $intitule = array('Site ecommerce', 'Site vitrine', 'Intranet');

    //livre
    $livre = array(false, true);


    //table pour géré les coup de production
    $tabMetier = array();
    $tabEmploye = array();
    $tabProjet = array();


    //création des métiers
    for($i = 0; $i <= 3; $i++){
      $nouveauMetier = new Metier();

      $nouveauMetier->setIntituleMetier($metier[$i]);
      array_push($tabMetier,$nouveauMetier);

      $manager->persist($nouveauMetier);
    }

    //création des employes
    for ($i = 0; $i <= 20; $i++){
      $employe = new Employe();

      $rand1 = rand(0,10);
      $rand2 = rand(0,10);
      $employe->setNom($nom[$rand1]);
      $employe->setPrenom($prenom{$rand2});
      $employe->setEmail($nom[$rand1].'.'.$prenom[$rand2].'@gmail.com');
      $employe->setCoutJournalier(rand(25,45));
      $employe->setMetier($tabMetier[rand(0,3)]);
      $employe->setArchive(false);
      array_push($tabEmploye, $employe);

      $manager->persist($employe);
    }

    //création des projets
    for ($i = 0; $i <= 5; $i++){
      $projet = new Projet();

      $projet->setIntituleProjet($intitule[rand(0,2)]);
      $projet->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.");
      $projet->setType($type[rand(0,1)]);
      $projet->setLivre($livre[rand(0,1)]);
      $projet->setArchiveProjet(false);
      array_push($tabProjet, $projet);

      $manager->persist($projet);
    }

    //Création des Coup de Production
    for($i = 0; $i <= 100; $i++){
    $coup = new CoupProduction();

    $coup->setProjet($tabProjet[rand(0,5)]);
    $coup->setEmploye($tabEmploye[rand(0,10)]);
    $coup->setJourProduction(rand(1,30));

    $manager->persist($coup);
    }

    $manager->flush();

  }

}

 ?>
