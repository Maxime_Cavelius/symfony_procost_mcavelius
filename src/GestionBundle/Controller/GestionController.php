<?php

namespace GestionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use GestionBundle\Entity\Metier;
use GestionBundle\Form\MetierType;

use GestionBundle\Entity\Employe;
use GestionBundle\Form\EmployeType;

use GestionBundle\Entity\Projet;
use GestionBundle\Form\ProjetType;

class GestionController extends Controller
{

  // Début Index
    /**
     * @Route("/", name="cms_index")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();

      $employe = $em->getRepository('GestionBundle:Employe')->employeEnregistre();

      $projetRealisation = $em->getRepository('GestionBundle:Projet')->projetEnRealisation();

      $projetLivre = $em->getRepository('GestionBundle:Projet')->projetLivre();
      $projetNonLivre = $em->getRepository('GestionBundle:Projet')->projetNonLivre();

      $jourProduction = $em->getRepository('GestionBundle:CoupProduction')->allJourProduction();

      $opex = $em->getRepository('GestionBundle:Projet')->projetOpex();
      $capex = $em->getRepository('GestionBundle:Projet')->projetCapex();

      $rentibilite = $em->getRepository('GestionBundle:Projet')->projetRentabilite($capex,$opex);

      $livraison = $em->getRepository('GestionBundle:Projet')->projetLivraison($projetLivre,$projetNonLivre);

      $meillerEmploye = $em->getRepository('GestionBundle:CoupProduction')->meillerEmploye($projetLivre,$projetNonLivre);

      $lastProjet = $em->getRepository('GestionBundle:CoupProduction')->lastProjet();

      $lastTempSaisie = $em->getRepository('GestionBundle:CoupProduction')->lastTempSaisie();

      return $this->render('default/index.html.twig', array(
        'employe'           => $employe,
        'projetRealisation' => $projetRealisation,
        'projetLivre'       => $projetLivre,
        'jourProduction'    => $jourProduction,
        'rentabilite'       => $rentibilite,
        'livraison'         => $livraison,
        'meillerEmploye'    => $meillerEmploye,
        'lastProjet'        => $lastProjet,
        'lastTempSaisie'    => $lastTempSaisie,));
    }

// Fin Index

// DébutEmploye

    /**
     * @Route("/employe", name="cms_employe")
     */
    public function employeAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $employe = $em->getRepository('GestionBundle:Employe')->allEmploye();

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
        $employe,
        $request->query->getInt('page',1),
        10
      );

      return $this->render('employe/employe.html.twig', array(
        'pagination'        => $pagination,
      ));
    }

    /**
     * @Route("/employe_edit", name="cms_employeEdit")
     */
    public function employeEditAction(Request $request)
    {
      $employe = new Employe();
      $form = $this->createForm(EmployeType::class, $employe);

      $form->handleRequest($request);

      if($form->isSubmitted()){
        $em = $this->getDoctrine()->getManager();
        $em->persist($employe);
        $em->flush();

        return $this->redirect($this->generateUrl('cms_employe'));
      }

      return $this->render('employe/employe_edit.html.twig', array(
        'form' => $form->createView()
      ));
    }

    /**
     * @Route("/employe_update", name="cms_employeUpdate")
     */
    public function employeUpdateAction($id, Request $request)
    {
      return $this->render('employe/employe_update.html.twig');
    }

    /**
     * @Route("/employe_tracktime/{id}",requirements={"id" = "\d+"}, name="cms_employeTrack")
     */
    public function employeTrackAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $suiviEmploye = $em->getRepository('GestionBundle:Employe')->suiviEmploye($id);

      $suiviEmployeProject = $em->getRepository('GestionBundle:Employe')->suiviEmployeProjet($id);

      $projetToEmploye = $em->getRepository('GestionBundle:Projet')->projetToEmploye($id);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
        $suiviEmployeProject,
        $request->query->getInt('page',1),
        10
      );
      return $this->render('employe/employe_timetrack.html.twig', array(
        'suiviEmploye'      => $suiviEmploye,
        'projetToEmploye'   => $projetToEmploye,
        'pagination'        => $pagination,));
    }

    /**
     * @Route("/employe_delete/{id}", name="cms_employeDelete")
     */
    public function employeDeleteAction(Merier $id, Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $em->remove($metier);
      $em->flush();
    }

    // Fin employe

    // Début projet

    /**
     * @Route("/projet", name="cms_projet")
     */
    public function projetAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $projet = $em->getRepository('GestionBundle:Projet')->allProjet();

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
        $projet,
        $request->query->getInt('page',1),
        10
      );

      return $this->render('projet/projet.html.twig', array(
        'pagination'        => $pagination,
      ));
    }

    /**
     * @Route("/projet_edit", name="cms_projetEdit")
     */
    public function projetEditAction(Request $request)
    {
      $projet = new Projet();
      $form = $this->createForm(ProjetType::class, $projet);

      $form->handleRequest($request);

      if($form->isSubmitted()){
        $em = $this->getDoctrine()->getManager();
        $em->persist($projet);
        $em->flush();

        return $this->redirect($this->generateUrl('cms_projet'));
      }

      return $this->render('projet/projet_edit.html.twig', array(
        'form' => $form->createView()
      ));
    }

    /**
     * @Route("/projet_update", name="cms_projetUpdate")
     */
    public function projetUpdateAction(Request $request)
    {
      //return $this->render('projet/projet_update.html.twig');
    }

    /**
     * @Route("/projet_tracktime/{id}",requirements={"id" = "\d+"}, name="cms_projetTrack")
     */
    public function projetTrackAction($id, Request $request)

    {
      $em = $this->getDoctrine()->getManager();

      $projetById = $em->getRepository('GestionBundle:Projet')->projetById($id);

      $projetTempProd = $em->getRepository('GestionBundle:Projet')->projetTempProd($id);

      $projetEmploye = $em->getRepository('GestionBundle:Projet')->projetEmploye($id);

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
        $projetTempProd,
        $request->query->getInt('page',1),
        10
      );
      return $this->render('projet/projet_timetrack.html.twig', array(
        'projetById'        => $projetById,
        'projetEmploye'    => $projetEmploye,
        'pagination'        => $pagination,));
    }

    /**
     * @Route("/projete_delete", name="cms_projetDelete")
     */
    public function projetDeleteAction($id, Request $request)
    {
      return $this->render('projet/projet_delete.html.twig');
    }

    // Fin projet

    // Début métiermetier

    /**
     * @Route("/metier", name="cms_metier")
     */
    public function metierAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $metiers = $em->getRepository('GestionBundle:Metier')->allMetier();

      $paginator = $this->get('knp_paginator');
      $pagination = $paginator->paginate(
        $metiers,
        $request->query->getInt('page',1),
        10
      );


      return $this->render('metier/metier.html.twig', array(
        'pagination'        => $pagination,
      ));
    }

    /**
     * @Route("/metier_edit", name="cms_metierEdit")
     */
    public function metierEditAction(Request $request)
    {
      $metier = new Metier();
      $form = $this->createForm(MetierType::class, $metier);

      $form->handleRequest($request);

      if($form->isSubmitted()){
        $em = $this->getDoctrine()->getManager();
        $em->persist($metier);
        $em->flush();

        return $this->redirect($this->generateUrl('cms_metier'));
      }

      return $this->render('metier/metier_edit.html.twig', array(
        'form' => $form->createView()
      ));
    }

    /**
     * @Route("/metier_update", name="cms_metierUpdate")
     */
    public function metierUpdateAction($id, Request $request)
    {
      return $this->render('metier/metier_update.html.twig');
    }

    /**
     * @Route("/metier_delete/{id}", requirements={"id" = "\d+"} , name="cms_metierDelete")
     */
    public function metierDeleteAction($id, Request $request)
    {
      return $this->render('metier/metier_delete.html.twig');
    }

    // Fin métier
}
